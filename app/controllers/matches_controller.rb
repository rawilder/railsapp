class MatchesController < ApplicationController
  http_basic_authenticate_with name: Rails.application.secrets.admin_username, password: Rails.application.secrets.admin_password, except: [:index, :show]
  
  add_breadcrumb "Matches", :matches_path
  
  def index
    # @matches = Match.all
    @matches = Match.paginate(:page => params[:page], :per_page => 10)
  end
  
  def show
    @match = Match.find(params[:id])
    add_breadcrumb @match.name, match_path(@match)
  end
  
  def new
      
  end
  
  def create
    @match = Match.new(match_params)
    @match.save
    redirect_to @match
  end
  
  def destroy
    match = Match.find(params[:id])
    
    player1 = match.player1
    player2 = match.player2
    victor = match.victor
    
    p1_won = (victor.id == player1.id)
    p1_matches_played = player1.matches_played - 1
    if p1_matches_played == 0
      player1.destroy
    else
      p1_matches_won = player1.matches_won
      p1_matches_won += p1_won ? -1 : 0
      p1_matches_lost = p1_matches_played - p1_matches_won
      p1_win_rate = BigDecimal.new(p1_matches_won)/BigDecimal.new(p1_matches_played)
      player1.update_attributes({ :matches_played => p1_matches_played, :matches_won => p1_matches_won, :matches_lost => p1_matches_lost, :win_rate => p1_win_rate })
    end
    
    print "got here"
    
    p2_matches_played = player2.matches_played - 1
    if p2_matches_played == 0
      player2.destroy
    else
      p2_matches_won = player2.matches_won
      p2_matches_won += !p1_won ? -1 : 0
      p2_matches_lost = p2_matches_played - p2_matches_won
      p2_win_rate = BigDecimal.new(p2_matches_won)/BigDecimal.new(p2_matches_played)
      player2.update_attributes({ :matches_played => p2_matches_played, :matches_won => p2_matches_won, :matches_lost => p2_matches_lost, :win_rate => p2_win_rate })
    end
    
    match.destroy
    flash[:success] = "Match deleted"
    redirect_to matches_path
  end
  
  private
  def match_params
    params.require(:match).permit(:victor_id, :player1_id, :player2_id)
  end
    
end
