class WebcallsController < ApplicationController

  def record_match
      
    player1=Character.find_by_name(params[:player1])
    if !player1
      player1 = Character.new({ :name => params[:player1], :matches_played => 0, :matches_won => 0, :matches_lost => 0, :win_rate => 0 })
      player1.save
    end
    
    player2=Character.find_by_name(params[:player2])
    if !player2
      player2 = Character.new({ :name => params[:player2], :matches_played => 0, :matches_won => 0, :matches_lost => 0, :win_rate => 0 })
      player2.save
    end
    
    victor = Character.find_by_name(params[:victor])
    
    p1_won = (victor.id == player1.id)
    p1_matches_played = player1.matches_played + 1
    p1_matches_won = player1.matches_won
    p1_matches_won += p1_won ? 1 : 0
    p1_matches_lost = p1_matches_played - p1_matches_won
    p1_win_rate = BigDecimal.new(p1_matches_won)/BigDecimal.new(p1_matches_played)
    player1.update_attributes({ :matches_played => p1_matches_played, :matches_won => p1_matches_won, :matches_lost => p1_matches_lost, :win_rate => p1_win_rate })
    
    print "got here"

    p2_matches_played = player2.matches_played + 1
    p2_matches_won = player2.matches_won
    p2_matches_won += !p1_won ? 1 : 0
    p2_matches_lost = p2_matches_played - p2_matches_won
    p2_win_rate = BigDecimal.new(p2_matches_won)/BigDecimal.new(p2_matches_played)
    player2.update_attributes({ :matches_played => p2_matches_played, :matches_won => p2_matches_won, :matches_lost => p2_matches_lost, :win_rate => p2_win_rate })
    
    match_hash = { :player1_id => player1.id, :player2_id => player2.id, :victor_id => victor.id }
    
    match = Match.new(match_hash)
    match.save
      
  end
    
  def current_match
    
    # Rails.application.config.player1 = params[:player1]
    # Rails.application.config.player2 = params[:player2]
    
    new_current_match = { :player1_name => params[:player1], :player2_name => params[:player2] }
    current_match = CurrentMatch.find_or_create_by(id: 1)
    current_match.update_attributes(new_current_match)
  
  end

end
