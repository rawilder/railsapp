class CharactersController < ApplicationController
  http_basic_authenticate_with name: Rails.application.secrets.admin_username, password: Rails.application.secrets.admin_password, except: [:index, :show]
  helper_method :sort_column, :sort_direction
  
  add_breadcrumb "Characters", :characters_path
  
  def index
    # @characters = Character.all
    # @characters = Character.paginate(:page => params[:page], :per_page => 30)
    if sort_column == "win_rate"
      @characters = Character.search(params[:search]).order(sort_column + " " + sort_direction,  "matches_played desc").paginate(:per_page => 10, :page => params[:page])
    elsif sort_column == "matches_played"
      @characters = Character.search(params[:search]).order(sort_column + " " + sort_direction,  "win_rate desc").paginate(:per_page => 10, :page => params[:page])
    elsif sort_column == "matches_won"
      @characters = Character.search(params[:search]).order(sort_column + " " + sort_direction,  "win_rate desc").paginate(:per_page => 10, :page => params[:page])
    elsif sort_column == "matches_lost"
      @characters = Character.search(params[:search]).order(sort_column + " " + sort_direction,  "win_rate asc").paginate(:per_page => 10, :page => params[:page])
    else
      @characters = Character.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:per_page => 10, :page => params[:page])
    end
  end
  
  def show
    @character = Character.find(params[:id])
    @matchup_data = get_matchup_data(params[:id])
    p1_matchs = Match.group_by_week(:created_at, format: "%b %d, %Y").where(:player1_id => @character.id).count
    p2_matchs = Match.group_by_week(:created_at, format: "%b %d, %Y").where(:player2_id => @character.id).count
    victories = Match.group_by_week(:created_at, format: "%b %d, %Y").where(:victor_id => @character.id).count
    matches = p1_matchs.merge(p2_matchs){ |date, p1_match_count, p2_match_count| p1_match_count + p2_match_count }
    @win_rate_by_week = matches.merge(victories){ |date, match_count, victory_count| (BigDecimal.new(victory_count ? victory_count : 0)/BigDecimal.new(match_count)) }
    @win_rate_by_week = {}
    matches.each do |key, value|
      match_count = value
      victory_count = victories[key] ? victories[key] : 0
      @win_rate_by_week[key] = BigDecimal.new(victory_count)/BigDecimal.new(match_count)
    end
    @win_rate_by_week = Hash[@win_rate_by_week.sort.map {|date, win_rate| [date, win_rate]}]
  end
  
  def new
      
  end
  
  def create
    @character = Character.new(character_params)
    @character.save
    redirect_to @character
  end
  
  def destroy
    Character.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to characters_path
  end
  
  private
  def character_params
    params.require(:character).permit(:name)
  end
  
  def sort_column
    Character.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
end
