class MatchupController < ApplicationController
  autocomplete :character, :name
  
  def index
  end
  
  def show
  end
  
  def get_matchup
    @player1_name = params[:player1]
    @player2_name = params[:player2]
    @player1 = Character.find_by_name(@player1_name)
    @player2 = Character.find_by_name(@player2_name)
    @player1_matchup_data = @player1 && @player2 ? get_matchup_data(@player1.id, @player2.id) : nil
    @player2_matchup_data = @player2 && @player1 ? get_matchup_data(@player2.id, @player1.id) : nil
  end
  
  def get_names
    @characters = Character.search(params[:search]).order("name asc")
  end
    
end
