class WelcomeController < ApplicationController
  skip_before_filter :verify_authenticity_token
  # after_action :set_access_control_headers
  
  def index
    # @player1 = Character.find_by_name(Rails.application.config.player1)
    # @player2 = Character.find_by_name(Rails.application.config.player2)
    @current_match = CurrentMatch.first
    if @current_match
      @player1 = Character.find_by_name(@current_match.player1_name)
      @player2 = Character.find_by_name(@current_match.player2_name)
    else
      @player1 = nil
      @player2 = nil
    end
    @player1_matchup_data = @player1 && @player2 ? get_matchup_data(@player1.id, @player2.id) : nil
    @player2_matchup_data = @player2 && @player1 ? get_matchup_data(@player2.id, @player1.id) : nil

  end
  
  def record_match
  end
  
  private
  
  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = "*"
    headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
  end
  
end
