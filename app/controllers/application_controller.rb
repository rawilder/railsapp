class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  # before_filter :set_cache_headers
  
  add_breadcrumb "Home", :root_path
  
  def get_matchup_data(character_id, opponent_id=nil)
    characters = Character.all
    character = characters.find(character_id)
    player1_matches = opponent_id ? character.player1s.where(:player2_id => opponent_id ) : character.player1s
    player2_matches = opponent_id ? character.player2s.where(:player1_id => opponent_id ) : character.player2s
    matchup_data = {}
    player1_matches.each do |player1match|
      if !matchup_data[player1match.player2_id]
        matchup_data[player1match.player2_id] = { :name => characters.find(player1match.player2_id).name, :matches_played => 0, :matches_won => 0, :matches_lost => 0, :win_rate => 0 }
      end
      p1_won = (player1match.victor_id == player1match.player1_id)
      p1_matches_played = matchup_data[player1match.player2_id][:matches_played] + 1
      p1_matches_won = matchup_data[player1match.player2_id][:matches_won]
      p1_matches_won += p1_won ? 1 : 0
      p1_matches_lost = p1_matches_played - p1_matches_won
      p1_win_rate = BigDecimal.new(p1_matches_won)/BigDecimal.new(p1_matches_played)
      matchup_data[player1match.player2_id].deep_merge!({ :matches_played => p1_matches_played, :matches_won => p1_matches_won, :matches_lost => p1_matches_lost, :win_rate => p1_win_rate })
    end
    
    player2_matches.each do |player2match|
      if !matchup_data[player2match.player1_id]
        matchup_data[player2match.player1_id] = { :name => characters.find(player2match.player1_id).name, :matches_played => 0, :matches_won => 0, :matches_lost => 0, :win_rate => 0 }
      end
      p2_won = (player2match.victor_id == player2match.player2_id)
      p2_matches_played = matchup_data[player2match.player1_id][:matches_played] + 1
      p2_matches_won = matchup_data[player2match.player1_id][:matches_won]
      p2_matches_won += p2_won ? 1 : 0
      p2_matches_lost = p2_matches_played - p2_matches_won
      p2_win_rate = BigDecimal.new(p2_matches_won)/BigDecimal.new(p2_matches_played)
      matchup_data[player2match.player1_id].deep_merge!({ :matches_played => p2_matches_played, :matches_won => p2_matches_won, :matches_lost => p2_matches_lost, :win_rate => p2_win_rate })
    end
    
    return matchup_data.empty? ? nil : matchup_data
    
  end
  
  # def set_cache_headers
  #   response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
  #   response.headers["Pragma"] = "no-cache"
  #   response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  # end
  
end
