# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
    
  $("#get_matchup").click -> (
    player1 = $('#player1').val()
    player2 = $('#player2').val()
    console.log('/matchup/get_matchup/ ' + player1 + ' ' + player2)
    $.post '/matchup/get_matchup/', { player1: player1, player2: player2 }, null, 'script'
    false
  )
  
  # $('#player1').keyup ->
  #   console.log $('#player1').attr('action'), $('#characters_search').serialize()
  #   $.get $('#player1').attr('action'), $('#player1').serialize(), null, 'script'
  #   false

$(document).ready(ready)
$(document).on('page:load', ready)