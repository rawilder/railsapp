# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
    
  $("#transfer_to_prod").click ->
    $("tbody tr").each ->
      data = []
      $("td", this).each (i) ->
        data[i] = ($(this).text())
        return
      if (!data[0] || !data[1] || !data[2])
        console.log("null found")
      else
        console.log(data)
        $.post( "https://ruby-rawilder.rhcloud.com/webcalls/record_match", { player1: data[0], player2: data[1], victor: data[2] } );
      return 
    return
            
$(document).ready(ready)
$(document).on('page:load', ready)