// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
//= require jquery-ui/autocomplete
//= require autocomplete-rails
//= require turbolinks

// var ready = function() {
//   $("#characters").on('click', 'th a, .pagination a', function() {
//     $.getScript(this.href);
//     return false;
//   });
//   $("#characters_search input").keyup(function() {
//     console.log($("#characters_search").attr("action"), $("#characters_search").serialize())
//     $.get($("#characters_search").attr("action"), $("#characters_search").serialize(), null, "script");
//     return false;
//   });
// };

// $(document).ready(ready)
// $(document).on('page:load', ready)

$(document).on('page:change', function() {
  var $productContainer = $('#welcome_header');
  var productPolling;

  function fetchCurrentMatch() {
      $.getScript('/')
      false
  }
  
  if ($productContainer.length) {
    fetchCurrentMatch()
    productPolling = setInterval(fetchCurrentMatch, 5000);
    $(document).on('page:change', clearPolling);
  }

  function clearPolling() {
    clearInterval(productPolling);
    $(document).off('page:change', clearPolling);
  }
});

