# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->

  # reverseTable = ->
  #   rows = $('tbody tr').get().reverse()
  #   $.each rows, (index, row) ->
  #     $('table').children('tbody').append row
  #     return
  #   return
  
  # sortTable = (col) ->
  #   rows = $('tbody tr').get()
  #   rows.sort (a, b) ->
  #     A = $(a).children('td').eq(col).text().toUpperCase()
  #     B = $(b).children('td').eq(col).text().toUpperCase()
  #     if A < B
  #       return -1
  #     if A > B
  #       return 1
  #     0
  #   $.each rows, (index, row) ->
  #     $('table').children('tbody').append row
  #     return
  #   return
  
  # $('th').click ->
  #   if $(this).hasClass('sorted')
  #     reverseTable()
  #     $(this).find('span.order').toggleClass("dropdown dropup");
  #   else
  #     $('th.sorted').find('span').remove()
  #     $('th.sorted').removeClass 'sorted'
  #     col = $(this).parent().children().index($(this))
  #     $(this).addClass 'sorted'
  #     sortTable col
  #     $(this).append '<span class=\'order dropup\'><span class=\'caret\'></span></span>'
  #     if !$(this).hasClass 'text'
  #       reverseTable()
  #       $(this).find('span.order').toggleClass("dropdown dropup");
  #     return
  
  $('#characters').on 'click', 'th a, .pagination a', ->
    $.getScript @href
    false
  $('#characters_search input').keyup ->
    console.log $('#characters_search').attr('action'), $('#characters_search').serialize()
    $.get $('#characters_search').attr('action'), $('#characters_search').serialize(), null, 'script'
    false
        
$(document).ready(ready)
$(document).on('page:load', ready)