class Character < ActiveRecord::Base
    has_many :player1s, :class_name => "Match", :foreign_key => "player1_id", :dependent => :destroy
    has_many :player2s, :class_name => "Match", :foreign_key => "player2_id", :dependent => :destroy
    has_many :victors, :class_name => "Match", :foreign_key => "victor_id"
    
    def self.search(search)
      if search
        where('name LIKE ?', "%#{search}%")
      else
        all
      end
    end
    
end
