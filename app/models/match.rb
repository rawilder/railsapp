class Match < ActiveRecord::Base
  belongs_to :player1, :class_name => "Character"
  belongs_to :player2, :class_name => "Character"
  belongs_to :victor, :class_name => "Character"
  
end
