module ApplicationHelper
    
    def sortable(column, title = nil)
      title ||= column.titleize
      css_class = column == sort_column ? "current #{sort_direction}" : nil
      is_sort_column = column == sort_column
      is_name = column == "name"
      is_desc = sort_direction == "desc"
      if is_sort_column
        direction = is_sort_column && is_desc ? "asc" : "desc"
      else
        direction = is_sort_column || is_name  ? "asc" : "desc"
      end
      link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
    end
    
end
