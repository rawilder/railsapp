# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.1'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
Rails.application.config.assets.precompile += ["characters.js", "get_current_match.js", "get_matches.js", "matches.js", "matchup.js", "webcalls.js", "welcome.js", ]
