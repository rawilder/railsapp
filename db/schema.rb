# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160118235404) do

  create_table "characters", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "matches_played", limit: 4
    t.decimal  "win_rate",                   precision: 6, scale: 5
    t.integer  "matches_won",    limit: 4
    t.integer  "matches_lost",   limit: 4
  end

  add_index "characters", ["name"], name: "index_characters_on_name", unique: true, using: :btree

  create_table "current_match", force: :cascade do |t|
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "player1_name", limit: 255
    t.string   "player2_name", limit: 255
  end

  create_table "matches", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "player1_id", limit: 4
    t.integer  "player2_id", limit: 4
    t.integer  "victor_id",  limit: 4
  end

end
