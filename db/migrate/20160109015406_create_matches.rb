class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.timestamps null: false
    end
    add_column :matches, :player1_id, :integer
    add_column :matches, :player2_id, :integer
    add_column :matches, :victor_id, :integer
  end
end
