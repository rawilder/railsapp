class ChangeCharacterWinRateScale < ActiveRecord::Migration
  def change
    change_column :characters, :win_rate, :decimal, :precision => 6, :scale => 5
  end
end
