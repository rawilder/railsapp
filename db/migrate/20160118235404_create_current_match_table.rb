class CreateCurrentMatchTable < ActiveRecord::Migration
  def change
    create_table :current_match do |t|
      t.timestamps null: false
    end
    add_column :current_match, :player1_name, :string
    add_column :current_match, :player2_name, :string
  end
end
