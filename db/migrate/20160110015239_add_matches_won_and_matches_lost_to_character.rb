class AddMatchesWonAndMatchesLostToCharacter < ActiveRecord::Migration
  def change
    add_column :characters, :matches_won, :integer
    add_column :characters, :matches_lost, :integer
  end
end
