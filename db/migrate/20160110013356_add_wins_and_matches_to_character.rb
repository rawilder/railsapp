class AddWinsAndMatchesToCharacter < ActiveRecord::Migration
  def change
    add_column :characters, :matches_played, :integer
    add_column :characters, :win_rate, :decimal, precision: 6, scale: 6
  end
end
 